package me.mrkirby153.quarxel.manager.common;

import org.gradle.api.Project;

public class Strings {

    public static String DOWNLOAD_DIR = "{CACHE_DIR}/QuarxelManager";

    public static String resolve(String pattern, Project project) {
        project.getLogger().debug("Resolving: " + pattern);
        pattern = pattern.replace("{CACHE_DIR}", project.getGradle().getGradleUserHomeDir().getAbsolutePath().replace('\\', '/')+ "/caches");
        project.getLogger().debug("Resolved to: "+pattern);
        return pattern;
    }
}
