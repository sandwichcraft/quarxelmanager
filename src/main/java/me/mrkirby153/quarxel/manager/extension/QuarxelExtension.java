package me.mrkirby153.quarxel.manager.extension;

import org.gradle.api.internal.java.JavaLibrary;

public class QuarxelExtension {

    protected String serverLocation = "";
    protected String serverArgs = "";

    protected JavaLibrary uploadArtifact;

    public String getServerArgs() {
        return serverArgs;
    }

    public void setServerArgs(String serverArgs) {
        this.serverArgs = serverArgs;
    }

    public String getServerLocation() {
        return serverLocation;
    }

    public void setServerLocation(String serverLocation) {
        this.serverLocation = serverLocation;
    }

    public JavaLibrary getUploadArtifact() {
        return uploadArtifact;
    }

    public void setUploadArtifact(JavaLibrary uploadArtifact) {
        this.uploadArtifact = uploadArtifact;
    }
}
