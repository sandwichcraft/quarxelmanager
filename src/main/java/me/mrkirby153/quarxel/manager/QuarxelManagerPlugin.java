package me.mrkirby153.quarxel.manager;

import me.mrkirby153.quarxel.manager.extension.QuarxelExtension;
import me.mrkirby153.quarxel.manager.tasks.CreateServer;
import me.mrkirby153.quarxel.manager.tasks.RunBuildToolsTask;
import me.mrkirby153.quarxel.manager.tasks.UploadTask;
import me.mrkirby153.quarxel.manager.tasks.util.DownloadTask;
import me.mrkirby153.quarxel.manager.tasks.util.ExtractFromJarTask;
import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.PublishArtifact;
import org.gradle.api.internal.component.Usage;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.specs.Spec;
import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.JavaExec;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import java.io.File;
import java.io.InputStream;
import java.util.*;

public class QuarxelManagerPlugin implements Plugin<Project> {

    public Project project;

    @Override
    public void apply(Project project) {
        this.project = project;
        project.getExtensions().create("quarxel", QuarxelExtension.class);
        final QuarxelExtension obj = (QuarxelExtension) project.getExtensions().findByName("quarxel");
        project.getPluginManager().apply(JavaPlugin.class);
        project.afterEvaluate(new Action<Project>() {
            @Override
            public void execute(Project project) {
                afterEval(project, obj);
            }
        });
        File buildToolsLoc;
        {
            DownloadTask dt = makeTask("downloadBT", DownloadTask.class);
            dt.setUrl("https://hub.spigotmc.org/jenkins/view/All/job/BuildTools-Beta/lastSuccessfulBuild/artifact/target/BuildTools.jar");
            dt.setOutputFile(buildToolsLoc = new File(getGradleCache(), "BuildTools.jar"));
            dt.setGroup("QuarxelManager");
            dt.setDescription("Downloads Build Tools");
        }
        RunBuildToolsTask runBuildToolsTask = makeTask("runBuildTools", RunBuildToolsTask.class);
        {
            runBuildToolsTask.setWorkingDir(new File(project.getGradle().getGradleUserHomeDir().getAbsolutePath() + "/caches/QuarxelManager/BuildTools"));
            runBuildToolsTask.getWorkingDir().mkdirs();
            runBuildToolsTask.setBtLoc(buildToolsLoc);
            runBuildToolsTask.dependsOn("downloadBT");
            runBuildToolsTask.setOutput(new File(runBuildToolsTask.getWorkingDir(), "spigot-1.8.3.jar"));
            runBuildToolsTask.getOutputs().upToDateWhen(new Spec<Task>() {
                @Override
                public boolean isSatisfiedBy(Task task) {
                    File output = ((RunBuildToolsTask) task).getOutput();
                    if (!output.exists())
                        return false;
                    Calendar c = Calendar.getInstance();
                    c.setTimeInMillis(System.currentTimeMillis());
                    c.add(Calendar.WEEK_OF_YEAR, 14);
                    return output.lastModified() < c.getTimeInMillis();
                }
            });
        }
        {
            ExtractFromJarTask extractStartScript = makeTask("extractStartScript", ExtractFromJarTask.class);
            extractStartScript.setExtractLocation(new File(getGradleCache(), "start.bat"));
            InputStream startStream = QuarxelManagerPlugin.class.getResourceAsStream("/start.bat");
            extractStartScript.setInputStream(startStream);
        }
        {
            CreateServer createServer = makeTask("createServer", CreateServer.class);
            createServer.setGroup("QuarxelManager");
            createServer.setDescription("Creates a testing server");
            createServer.dependsOn("runBuildTools", "extractStartScript");
            createServer.setSpigotFile(runBuildToolsTask.getOutput());
        }
        {
           /* CopyTask task = makeTask("deployTestPlugin", CopyTask.class);
            AbstractArchiveTask jarTask = (AbstractArchiveTask) project.getTasks().getByName("jar");
            task.setSource(jarTask.getArchivePath());
            task.dependsOn("jar");*/
            Copy task = makeTask("deployTestPlugin", Copy.class);
            task.dependsOn("build");
            task.setGroup("QuarxelManager");
            task.setDescription("Deploys the plugin to the local test server");
        }
        {
            JavaExec start = makeTask("runServer", JavaExec.class);
            start.setMain("-jar");
            start.setStandardOutput(System.out);
            start.setStandardInput(System.in);
            start.setErrorOutput(System.err);
            start.setGroup("QuarxelManager");
            start.setDescription("[NOT SUPPORTED] Runs the local test server");

        }
        {
            UploadTask upload = makeTask("upload", UploadTask.class);
            upload.dependsOn("build");
            upload.setGroup("QuarxelManager");
            upload.setDescription("Uploads the plugin to the remote test server");
        }
    }

    @SuppressWarnings("unchecked")
    public <T extends Task> T makeTask(String name, Class<T> type) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("name", name);
        map.put("type", type);
        return (T) project.task(map, name);
    }

    public static void afterEval(Project project, final QuarxelExtension ext) {
        {
            CreateServer cst = ((CreateServer) project.getTasks().getByName("createServer"));
            final File serverFile = new File("." + File.separator + ext.getServerLocation());
            cst.setServerOutputDirectory(serverFile);
            cst.setSpigotOutputFile(new File(cst.getServerOutputDirectory(), "spigot.jar"));
            cst.setStartFileOutput(new File(cst.getServerOutputDirectory(), "start.bat"));
            cst.setStartFileInput(new File(project.getGradle().getGradleUserHomeDir().getAbsolutePath() + "/caches/QuarxelManager", "start.bat"));

            Copy ct = (Copy) project.getTasks().getByName("deployTestPlugin");
            File pluginDest = new File(serverFile, "plugins");
            ct.into(pluginDest);
            ct.from(((AbstractArchiveTask) project.getTasks().getByName("jar")).getArchivePath());

            JavaExec exec = (JavaExec) project.getTasks().getByName("runServer");
            List<String> jvmArgs = Arrays.asList(ext.getServerArgs().split(" "));
            exec.jvmArgs(jvmArgs);
            exec.args("spigot.jar");
            exec.workingDir(ext.getServerLocation());

            UploadTask upload = (UploadTask) project.getTasks().getByName("upload");
            if (project.hasProperty("sftp_host"))
                upload.setHost(project.property("sftp_host").toString());
            else
                upload.setHost("INVALID");
            if (project.hasProperty("sftp_keyLoc")) {
                upload.setKeyLoc(project.property("sftp_keyLoc").toString());
                upload.setUsePubKey(true);
                upload.setPassword("USING_PUBLIC_KEY");
            } else {
                if (project.hasProperty("sftp_password"))
                    upload.setPassword(project.property("sftp_password").toString());
                else
                    upload.setPassword("INVALID");
                upload.setKeyLoc("PASSWORD");
                upload.setUsePubKey(false);
            }
            if (project.hasProperty("sftp_username"))
                upload.setUsername(project.property("sftp_username").toString());
            else
                upload.setUsername("INVALID");
            if (project.hasProperty("sftp_uploadLoc"))
                upload.setUploadLocation(project.property("sftp_uploadLoc").toString());
            else
                upload.setUploadLocation("INVALID");
            if (ext.getUploadArtifact() == null)
                upload.addArtifact(((AbstractArchiveTask) project.getTasks().getByName("jar")).getArchivePath());
            else {
                Set<Usage> u = ext.getUploadArtifact().getUsages();
                for (Usage us : u) {
                    for (PublishArtifact a : us.getArtifacts()) {
                        upload.addArtifact(a.getFile());
                    }
                }
            }

        }
    }

    public File getGradleCache() {
        return new File(project.getGradle().getGradleUserHomeDir().getAbsolutePath() + "/caches/QuarxelManager");
    }
}
