package me.mrkirby153.quarxel.manager.tasks.util;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadTask extends DefaultTask {

    @Input
    private String url;

    @OutputFile
    private File outputFile;

    @TaskAction
    public void doTask() throws Exception {
        getLogger().info("Downloading " + getUrl() + " to " + getOutputFile().getAbsolutePath());

        HttpURLConnection connection = (HttpURLConnection) (new URL(getUrl()).openConnection());
        connection.setRequestProperty("User-Agent", "QuarxelManager");
        connection.setInstanceFollowRedirects(true);

        InputStream urlInputStream = connection.getInputStream();
        OutputStream fileOutputStream = new FileOutputStream(getOutputFile());
        int data = urlInputStream.read();

        while (data != -1) {
            fileOutputStream.write(data);

            data = urlInputStream.read();
        }

        urlInputStream.close();
        fileOutputStream.flush();
        fileOutputStream.close();

        getLogger().info("Download complete!");
    }

    public File getOutputFile() {
        return this.outputFile;
    }

    public void setOutputFile(File file) {
        this.outputFile = file;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
