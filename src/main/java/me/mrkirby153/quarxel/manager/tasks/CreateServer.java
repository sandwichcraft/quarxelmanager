package me.mrkirby153.quarxel.manager.tasks;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;

public class CreateServer extends DefaultTask {

    public File getSpigotFile() {
        return spigotFile;
    }

    public void setSpigotFile(File spigotFile) {
        this.spigotFile = spigotFile;
    }

    public File getServerOutputDirectory() {
        return serverOutputDirectory;
    }

    public void setServerOutputDirectory(File serverOutputDirectory) {
        this.serverOutputDirectory = serverOutputDirectory;
    }


    public File getSpigotOutputFile() {
        return spigotOutputFile;
    }

    public void setSpigotOutputFile(File spigotOutputFile) {
        this.spigotOutputFile = spigotOutputFile;
    }

    public File getStartFileOutput() {
        return startFileOutput;
    }

    public void setStartFileOutput(File startFileOutput) {
        this.startFileOutput = startFileOutput;
    }

    public File getStartFileInput() {
        return startFileInput;
    }

    public void setStartFileInput(File startFileInput) {
        this.startFileInput = startFileInput;
    }

    @TaskAction
    public void doTask(){
        try {
            OutputStream outputDir = new FileOutputStream(spigotOutputFile);
            Files.copy(getSpigotFile().toPath(), outputDir);
            outputDir.close();
            OutputStream outputStartScript = new FileOutputStream(startFileOutput);
            Files.copy(startFileInput.toPath(), outputStartScript);
            outputStartScript.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @InputFile
    private File spigotFile;

    @InputFile
    private File startFileInput;

    @OutputDirectory
    private File serverOutputDirectory;

    @OutputFile
    private File spigotOutputFile;

    @OutputFile
    private File startFileOutput;

}
