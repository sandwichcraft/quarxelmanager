package me.mrkirby153.quarxel.manager.tasks;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;

public class RunServer extends DefaultTask {


    private String serverArgs;

    private File workingDir;

    public String getServerArgs() {
        return serverArgs;
    }

    public void setServerArgs(String serverArgs) {
        this.serverArgs = serverArgs;
    }

    public File getWorkingDir() {
        return workingDir;
    }

    public void setWorkingDir(File workingDir) {
        this.workingDir = workingDir;
    }


    @TaskAction
    public void doTask() {

    }
}
