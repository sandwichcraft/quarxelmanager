package me.mrkirby153.quarxel.manager.tasks;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UploadTask extends DefaultTask {

    @InputFile
    private List<File> artifact;

    @Input
    private String keyLoc;

    @Input
    private String username;

    @Input
    private String password;

    @Input
    private boolean usePubKey;

    @Input
    private String host;

    public String getUploadLocation() {
        return uploadLocation;
    }

    public void setUploadLocation(String uploadLocation) {
        this.uploadLocation = uploadLocation;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKeyLoc() {
        return keyLoc;
    }

    public void setKeyLoc(String keyLoc) {
        this.keyLoc = keyLoc;
    }

    public List<File> getArtifacts() {
        return artifact;
    }

    public void setArtifact(List<File> artifact) {
        this.artifact = artifact;
    }

    public void addArtifact(File file) {
        if (this.artifact == null)
            artifact = new ArrayList<File>();
        this.artifact.add(file);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean usePubKey() {
        return usePubKey;
    }

    public void setUsePubKey(boolean usePubKey) {
        this.usePubKey = usePubKey;
    }

    @Input
    private String uploadLocation;


    @TaskAction
    public void doTask() throws Exception {

        if(!validSettings()){
            getLogger().info("Not uploading due to unspecified settings!");
            return;
        }
        getLogger().info("Uploading ");
        for (File f : getArtifacts()) {
            getLogger().info("\t+ " + f.getAbsolutePath());
        }
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        JSch jsch = new JSch();
        if (usePubKey()) {
            getLogger().info("Using public key " + getKeyLoc());
            jsch.addIdentity(getKeyLoc());
        } else {
            getLogger().info("Using password " + getPassword());
        }
        Session session = jsch.getSession(getUsername(), getHost(), 22);
        session.setConfig(config);
        if (!usePubKey()) {
            session.setPassword(getPassword());
            UserInfo userInfo = new SftpUserInfo();
            session.setUserInfo(userInfo);
        }
        session.connect();

        com.jcraft.jsch.Channel channel = session.openChannel("sftp");
        getLogger().info("Connecting to " + getHost());
        channel.connect();

        ChannelSftp sftpChannel = (ChannelSftp) channel;
        getLogger().info("Changing directory to " + uploadLocation);
        sftpChannel.cd(uploadLocation);
        for (File f : getArtifacts()) {
            getLogger().info("Uploading " + f.getAbsolutePath());
            sftpChannel.put(new FileInputStream(f), f.getName());
        }

        System.out.println("File transfer complete!");
        // Close channel
        sftpChannel.exit();
        session.disconnect();
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    private boolean validSettings(){
        return !(getHost().equalsIgnoreCase("INVALID") || getPassword().equalsIgnoreCase("INVALID") || getUsername().equalsIgnoreCase("INVALID") || getUploadLocation().equalsIgnoreCase("INVALID"));
    }

    class SftpUserInfo implements UserInfo {

        String password = null;

        @Override
        public String getPassphrase() {
            return null;
        }

        @Override
        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public boolean promptPassword(String message) {
            return false;
        }

        @Override
        public boolean promptPassphrase(String message) {
            return false;
        }

        @Override
        public boolean promptYesNo(String message) {
            return false;
        }

        @Override
        public void showMessage(String message) {

        }
    }
}
