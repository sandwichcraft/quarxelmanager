package me.mrkirby153.quarxel.manager.tasks;


import groovy.lang.Closure;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.InputDirectory;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;
import org.gradle.process.ExecSpec;

import java.io.*;
import java.nio.file.Files;

public class RunBuildToolsTask extends DefaultTask {

    @InputDirectory
    private File workingDir;

    @InputFile
    private File btLoc;

    @OutputFile
    private File output;


    public File getWorkingDir() {
        return workingDir;
    }

    public void setWorkingDir(File workingDir) {
        this.workingDir = workingDir;
    }

    public File getOutput() {
        return output;
    }

    public void setOutput(File output) {
        this.output = output;
    }

    @TaskAction
    public void doTask(){
        try {
            OutputStream btOutput = new FileOutputStream(new File(getWorkingDir(), "BuildTools.jar"));
            Files.copy(getBtLoc().toPath(), btOutput);
            btOutput.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("Running BuildTools. THIS COULD TAKE A VERY LONG TIME!!!");
        System.out.println("Go get yourself a bite to eat or something");
        final String javaHomeLoc = System.getenv("JAVA_HOME");
        getProject().exec(new Closure<ExecSpec>(this) {
            @Override
            public ExecSpec call() {
                ExecSpec spec = (ExecSpec) getDelegate();
                spec.workingDir(getWorkingDir());
                spec.commandLine("cmd", "/c", "java -jar BuildTools.jar");
                spec.setStandardOutput(System.out);
                return spec;
            }
        });
    }

    public File getBtLoc() {
        return btLoc;
    }

    public void setBtLoc(File btLoc) {
        this.btLoc = btLoc;
    }
}
