package me.mrkirby153.quarxel.manager.tasks.util;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.*;

public class ExtractFromJarTask extends DefaultTask{
    public File getExtractLocation() {
        return extractLocation;
    }

    public void setExtractLocation(File extractLocation) {
        this.extractLocation = extractLocation;
    }

    @TaskAction
    public void doTask() throws Exception{
        FileOutputStream outputStream = new FileOutputStream(getExtractLocation());
        int curr;
        while((curr = getInputStream().read()) != -1){
            outputStream.write(curr);
        }
        outputStream.flush();
        outputStream.close();
    }

    @Input
    private InputStream inputFile;

    @OutputFile
    private File extractLocation;

    public InputStream getInputStream() {
        return inputFile;
    }

    public void setInputStream(InputStream inputFile) {
        this.inputFile = inputFile;
    }
}
