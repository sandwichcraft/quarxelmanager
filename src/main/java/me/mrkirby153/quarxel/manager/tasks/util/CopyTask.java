package me.mrkirby153.quarxel.manager.tasks.util;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.*;

public class CopyTask extends DefaultTask {

    @InputFile
    private File source;

    @OutputFile
    private File dest;
    public File getDest() {
        return dest;
    }

    public void setDest(File dest) {
        this.dest = dest;
    }

    public File getSource() {
        return source;
    }

    public void setSource(File source) {
        this.source = source;
    }

    @TaskAction
    public void doTask() throws Exception{
        InputStream inputStream = new FileInputStream(getSource());
        OutputStream outputStream = new FileOutputStream(getDest());
        int currByte;
        while((currByte = inputStream.read()) != -1){
            outputStream.write(currByte);
        }
        outputStream.flush();
        inputStream.close();
        outputStream.close();
    }

}
